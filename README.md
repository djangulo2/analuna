# README.md

Django application & CMS for Ana Luna's website: [integralpsychology.life](http://integralpsychology.life)

Built using:

* [Django-CMS](https://github.com/divio/django-cms)
* [Django-CMS Blog](https://github.com/nephila/djangocms-blog)
* [Bootstrap4](https://getbootstrap.com/)
