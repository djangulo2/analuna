from django.db import models
from django.utils.translation import gettext_lazy as _


class Contact(models.Model):
    first_name = models.CharField(
        verbose_name=_('First name'),
        max_length=50,
        blank=False,
        null=False,
    )
    last_name = models.CharField(
        verbose_name=_('Last name'),
        max_length=50,
        blank=False,
        null=False,
    )
    email = models.EmailField(
        verbose_name=_('Email'),
        blank=False,
        null=False,
    )
    phone = models.CharField(
        verbose_name=_('Phone number'),
        max_length=15,
        blank=True,
        null=True,
    )
    created_at = models.DateTimeField(auto_now_add=True, editable=False)

    class Meta:
        verbose_name = _('contact')
        verbose_name_plural = _('contacts')

    
    def get_messages(self):
        return self.messages.all()

    @property
    def get_message_count(self):
        return self.messages.count()

    @property
    def get_full_name(self):
        return '%s %s' % (self.first_name, self.last_name,)

    @property
    def get_latest_message(self):
        return self.messages.latest()

    def __str__(self):
        return f'{self.first_name} {self.last_name}: {self.email}'


class ContactMessage(models.Model):
    contact = models.ForeignKey(
        'contact.Contact',
        on_delete=models.CASCADE,
        blank=False,
        null=True,
        related_name='messages',
    )
    description = models.TextField(verbose_name=_('Description'))
    sent_at = models.DateTimeField(auto_now_add=True, editable=False)

    class Meta:
        verbose_name = _('contact message')
        verbose_name_plural = _('contact messages')
        get_latest_by = 'sent_at'
        ordering = ('-sent_at',)
