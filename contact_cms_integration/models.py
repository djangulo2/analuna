from django.db import models
from cms.models import CMSPlugin

from contact.models import Contact


class ContactPluginModel(CMSPlugin):
    contact = models.ForeignKey(Contact)

    def __unicode__(self):
        return self.contact.email
