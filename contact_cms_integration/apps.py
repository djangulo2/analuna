from django.apps import AppConfig


class ContactCmsIntegrationConfig(AppConfig):
    name = 'contact_cms_integration'
